/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 17:56:54 by akazian           #+#    #+#             */
/*   Updated: 2013/11/30 15:57:56 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

void	*ft_memset(void *b, int c, size_t len)
{
	char		*str_b;

	str_b = (char *) b;
	while (len != 0)
	{
		len--;
		str_b[len] = (unsigned char) c;
	}
	return (b);
}
