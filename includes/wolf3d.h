/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/02 12:20:08 by akazian           #+#    #+#             */
/*   Updated: 2014/01/19 17:11:12 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef _WOLF3D_H
# define _WOLF3D_H

# include <string.h>

# define IMG_X 1000
# define IMG_Y 600

# define K_ESC 65307
# define K_ENTER 65421
# define K_SHIFT 65506
# define K_UP 65362
# define K_DOWN 65364
# define K_RIGHT 65363
# define K_LEFT 65361
# define K_ADD 65451
# define K_SUB 65453

# define ABS(x) (((x) < 0) ? ((x) * -1) : (x))
# define FACT(n) ft_factorial(n)
# define POW(n, p) ft_power(n, p)
# define COS(n) ft_cosinus(n)
# define SIN(n) ft_sinus(n)
# define PI 3.141592654

typedef struct	s_env
{
	void		*mlx;
	void		*win;
	void		*img;
	char		*data;
	int			bpp;
	int			sizeline;
	int			endian;
	float		x;
	float		y;
	float		k;
	float		a;
	int			**map;
	int			mx;
	int			my;
	char		*map_name;
	char		*font;
}				t_env;

int		wolf_err(char *error_type, char *file);
void	input_font(t_env *e);
float	my_calcul_k(t_env *e, float x1, float y1);
void	print_wall(t_env *e);
void	insert_wall(t_env *e, int x, int color);
void	next_move(t_env *e, float x, float y);
void	put_color(t_env *e, int pos, int color, int h);
void	map_parser(t_env *e);
void	call_font(t_env *e);

/*
** math
*/

float	ft_factorial(float n);
float	ft_power(float n, int pow);
float	ft_cosinus(float n);
float	ft_sinus(float x);

#endif
