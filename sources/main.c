/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/31 15:16:00 by akazian           #+#    #+#             */
/*   Updated: 2014/01/19 17:23:58 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <mlx.h>
#include <wolf3d.h>
#include <stdlib.h>

static int	gere_key(unsigned int key_code, t_env *e)
{
	float	x;
	float	y;

	x = -1;
	y = -1;
	if (key_code == K_ESC)
		exit(0);
	if (key_code == K_UP)
	{
		x = e->x + (0.1 * COS((e->a / 180) * PI));
		y = e->y + (0.1 * SIN((e->a / 180) * PI));
	}
	if (key_code == K_DOWN)
	{
		x = e->x - (0.1 * COS((e->a / 180) * PI));
		y = e->y - (0.1 * SIN((e->a / 180) * PI));
	}
	if (key_code == K_RIGHT)
		e->a = e->a - 15;
	if (key_code == K_LEFT)
		e->a = e->a + 15;
	next_move(e, x, y);
	return (0);
}

static int	gere_expose(t_env *e)
{
	mlx_put_image_to_window(e->mlx, e->win, e->img, 0, 0);
	return (0);
}

static int	event_mlx(char *map)
{
	t_env	e;

	e.map_name = map;
	map_parser(&e);
	if (!(e.mlx = mlx_init()))
		return (1);
	if (!(e.win = mlx_new_window(e.mlx, IMG_X, IMG_Y, "WOLF3D")))
		return (1);
	e.img = mlx_new_image(e.mlx, IMG_X, IMG_Y);
	e.data = mlx_get_data_addr(e.img, &e.bpp, &e.sizeline, &e.endian);
	call_font(&e);
	input_font(&e);
	e.a = 180;
	e.k = -1;
	print_wall(&e);
	mlx_expose_hook(e.win, gere_expose, &e);
	mlx_hook(e.win, 2, 3, gere_key, &e);
	mlx_loop(e.mlx);
	return (0);
}

int			main(int argc, char **argv)
{
	if (argc != 2)
	{
		wolf_err("Usage: ./wolf3d map", NULL);
		return (1);
	}
	if (event_mlx(argv[1]))
		wolf_err("Can't create window", NULL);
	return (0);
}
