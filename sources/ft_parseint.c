/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parseint.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/18 14:36:07 by akazian           #+#    #+#             */
/*   Updated: 2014/01/19 21:02:37 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <wolf3d.h>
#include <libft.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>

static void	parse_exit(char *text)
{
	wolf_err(text, NULL);
	exit(1);
}

static void	add_value(t_env *e, int i, int j, char *line)
{
	if (line[j] == 'x')
	{
		e->map[i][j] = 0;
		e->x = (float)j + 0.5;
		e->y = (float)i + 0.5;
	}
	else
	e->map[i][j] = line[j] - '0';
}

static void	c_map(t_env *e)
{
	int	**map;

	map = (int **)ft_memalloc(sizeof(int *) * e->mx);
	e->map = map;
}

static void	save_map(t_env *e, int fd, char *line)
{
	int	*map2;
	int	i;
	int	j;
	int	rd;

	c_map(e);
	map2 = (int *)ft_memalloc(sizeof(int ) * e->my * e->mx);
	i = 0;
	while (i != e->my)
	{
		j = 0;
		if ((rd = get_next_line(fd, &line)) < 0 && line[0] == '\0')
			parse_exit("Can't read fd");
		e->map[i] = &map2[i * e->mx];
		while (j != e->mx)
		{
			if (line[j] && (line[j] == '0' || line[j] == '1' || line[j] == 'x'))
				add_value(e, i, j, line);
			else
				parse_exit("Wrong map format");
			j++;
		}
		i++;
		ft_strdel(&line);
	}
}

void		map_parser(t_env *e)
{
	int		fd;
	char	*line;
	int		rd;

	if ((fd = open(e->map_name, O_RDONLY)) < 0)
		parse_exit("Can't open fd");
	if ((rd = get_next_line(fd, &line)) < 0)
		parse_exit("Can't read fd");
	if (ft_strlen(line) > 2)
		e->mx = ft_atoi(line + 2);
	else
		parse_exit("Can't read fd");
	ft_strdel(&line);	
	if ((rd = get_next_line(fd, &line)) < 0)
		parse_exit("Can't read fd");
	if (ft_strlen(line) > 2)
		e->my = ft_atoi(line + 2);
	else
		parse_exit("Can't read fd");
	ft_strdel(&line);
	save_map(e, fd, line);
	close(fd);
}
