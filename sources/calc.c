/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calc.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/06 11:52:43 by akazian           #+#    #+#             */
/*   Updated: 2014/01/19 17:23:23 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <wolf3d.h>
#include <unistd.h>
#include <mlx.h>
#include <stdlib.h>

static float	calcul_k(t_env *e, float x1, float y1)
{
	int		x;
	int		y;
	float	k;

	k = 1;
	x = e->x + (k * (x1 - e->x));
	y = e->y + (k * (y1 - e->y));
	while (e->map[x][y] == 0)
	{
		x = e->x + (k * (x1 - e->x));
		y = e->y + (k * (y1 - e->y));
		k += 0.01;
	}
	return (k);
}

void			print_wall(t_env *e)
{
	float	a;
	float	tmp;
	float	x1;
	float	y1;
	float	i;

	input_font(e);
	a = ((e->a / 180) * PI);
	i = 0;
	while (i <= IMG_X)
	{
		x1 = 0.5;
		y1 = ((1 * ((IMG_X / 2) - i)) / IMG_X);
		tmp = x1;
		x1 = (tmp * COS(a)) - (y1 * SIN(a));
		y1 = (tmp * SIN(a)) + (y1 * COS(a));
		x1 = x1 + e->x;
		y1 = y1 + e->y;
		e->k = calcul_k(e, x1, y1);
		insert_wall(e, i , 0x4682b4);
		++i;
	}
	mlx_put_image_to_window(e->mlx, e->win, e->img, 0, 0);
}

void			next_move(t_env *e, float x, float y)
{
	int	a;
	int	b;

	if (e->a > 360)
		e->a -= 360;
	if (e->a < 0)
		e->a += 360;
	a = x;
	b = y;
	if (x >= 0 && x <= e->mx - 1 && y >= 0 && e->my - 1 && e->map[a][b] == 0)
	{
		e->x = x;
		e->y = y;
	}
	print_wall(e);
	mlx_put_image_to_window(e->mlx, e->win, e->img, 0, 0);
}
