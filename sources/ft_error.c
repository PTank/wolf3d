/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/02 15:39:42 by akazian           #+#    #+#             */
/*   Updated: 2014/01/02 16:07:49 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include <libft.h>

int		wolf_err(char *error_type, char *file)
{
	ft_putstr("wolf3d: ");
	if (file)
	{
		ft_putstr(file);
		ft_putstr(": ");
	}
	ft_putendl(error_type);
	return (1);
}
