/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cos.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/06 11:52:59 by akazian           #+#    #+#             */
/*   Updated: 2014/01/06 11:53:16 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <wolf3d.h>

float	ft_cosinus(float n)
{
	long int	i;
	float		cos1;
	float		cos2;

	i = 0;
	cos1 = 0;
	cos2 = 0;
	while (i < 15)
	{
		cos1 += POW(-1, i) / FACT(2 * i) * POW(n, 2 * i);
		cos2 += POW(-1, i) / FACT(2 * i) * POW(n + PI, 2 * i);
		i++;
	}
	return ((cos1 - cos2) / 2);
}
