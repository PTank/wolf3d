/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_factorial.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/06 11:53:14 by akazian           #+#    #+#             */
/*   Updated: 2014/01/17 14:01:14 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <wolf3d.h>

float	ft_factorial(float n)
{
	int		i;
	float	fact;

	if (n == 0)
		n = 1;
	fact = i = 1;
	while (i < n)
	{
		i++;
		fact *= i;
	}
	return (fact);
}
