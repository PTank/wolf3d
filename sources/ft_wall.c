/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wall.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/06 11:54:00 by akazian           #+#    #+#             */
/*   Updated: 2014/01/18 12:16:32 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <wolf3d.h>
#include <mlx.h>
#include <libft.h>

void	wall_down(t_env *e, int y, int h, int color)
{
	int	c;
	int	i;
	int	z;

	i = 0;
	while (i < h)
	{
		c = 0;
		while (c < (e->bpp / 8))
		{
			z = y + (i * e->sizeline);
			put_color(e, c + z, color, h);
			c += 3;
		}
		++i;
	}
}

void	insert_wall(t_env *e, int x, int color)
{
	int	i;
	int	h;
	int	c;
	int	y;
	int	z;

	h = (IMG_Y / (2 * e->k));
	y = ((IMG_Y / 2) * e->sizeline) + (x * e->bpp / 8);
	i = 0;
	while (i < h)
	{
		c = 0;
		while (c < (e->bpp / 8))
		{
			z = y - (i * e->sizeline);
			put_color(e, c + z, color, h);
			c += 3;
		}
		++i;
	}
	wall_down(e, y, h, color);
}

void	put_color(t_env *e, int pos, int color, int h)
{
	int	col;
	int	dark;

	dark = ((IMG_Y / 2) / h * 2);
	if (dark >= 50)
		dark = 50;
	col = mlx_get_color_value(e->mlx, color);
	if (pos >= 0 && pos >= e->sizeline)
	{
		e->data[pos + 0] = (col & 0xFF) - dark;
		e->data[pos + 1] = ((col & 0xFF00) >> 8) - dark;
		e->data[pos + 2] = ((col & 0xFF0000) >> 16) - dark;
	}
}
