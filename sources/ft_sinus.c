/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sinus.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/06 11:53:50 by akazian           #+#    #+#             */
/*   Updated: 2014/01/17 14:00:31 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <wolf3d.h>

float	ft_sinus(float x)
{
	int		n;
	float	sin1;
	float	sin2;

	n = 0;
	sin1 = 0;
	sin2 = 0;
	while (n < 15)
	{
		sin1 += POW(-1, n) / FACT(2 * n + 1) * POW(x, 2 * n + 1);
		sin2 += POW(-1, n) / FACT(2 * n + 1) * POW(-x, 2 * n + 1);
		n++;
	}
	return ((sin1 - sin2) / 2);
}
