/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_font.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/06 11:53:25 by akazian           #+#    #+#             */
/*   Updated: 2014/01/19 17:19:36 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <wolf3d.h>
#include <mlx.h>
#include <libft.h>

void	call_font(t_env *e)
{
	int		x;
	int		y;
	char	*img;

	x = 0;
	y = 0;
	img = mlx_xpm_file_to_image(e->mlx, "img/xoops.xpm", &x, &y);
	e->font = mlx_get_data_addr(img, &e->bpp, &e->sizeline, &e->endian);
}

void	input_font(t_env *e)
{
	int	i;

	i = 0;
	while (i != e->sizeline * IMG_Y)
	{
		e->data[i] = e->font[i];
		i++;
	}
}
