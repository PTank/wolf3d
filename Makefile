# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: akazian <akazian@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/12/16 16:00:35 by akazian           #+#    #+#              #
#    Updated: 2014/01/19 17:11:48 by akazian          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME			=	wolf3d
CC				=	gcc
CFLAGS			=	-Wall -Wextra -Werror -O3
LIBFTDIR		=	./libft/
LIBFTH			=	-I$(LIBFTDIR)
MLXDIR			=	./minilibx/
MLX				=	-I$(MLXDIR)
LIBXFLAGS		=	-L$(MLXDIR) -lmlx -L/usr/X11/lib/ -lXext -lX11 -lbsd
LIBFTFLAGS		=	-L$(LIBFTDIR) -lft
INCS_DIR		=	includes
OBJS_DIR		=	objects
SRCS_DIR		=	sources
SRCS			=	main.c\
					ft_error.c\
					ft_cos.c\
					ft_sinus.c\
					ft_factorial.c\
					ft_power.c\
					ft_font.c\
					ft_wall.c\
					calc.c\
					ft_parseint.c

OBJS 			=	$(patsubst %.c, $(OBJS_DIR)/%.o, $(SRCS))

all				:	$(NAME)

$(NAME)			:	$(OBJS_DIR) $(OBJS)
	$(CC) -o $(NAME) $(OBJS) $(LIBFTFLAGS) $(LIBXFLAGS)

$(OBJS_DIR)/%.o	:	$(addprefix $(SRCS_DIR)/, %.c)
	$(CC) $(CFLAGS) -o $@ -c $^ -I $(INCS_DIR) $(LIBFTH) $(MLX)

$(OBJS_DIR)	:	makelibft
	@mkdir -p $(OBJS_DIR)

makelibft:
	@make -C $(LIBFTDIR)
	@make -C $(MLXDIR)

.PHONY: clean all re fclean

fclean			:	clean
	rm -f $(NAME)

clean			:
	rm -rf $(OBJS_DIR)

re				:	fclean all
